using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App2.common;
using static Android.Widget.AdapterView;


namespace App2
{
    [Activity(Label = "BeverageList")]
    public class BeverageList : Activity
    {
        private ListView listView;
        private RadioGroup volumeGroup;
        private RadioButton volume;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            int maxCount;
            try
            {
                maxCount = Int32.Parse(Intent.GetStringExtra("MyData"));
            }
            catch (FormatException e) { maxCount = 0; }
            GetDrunk getDrunk = new GetDrunk(maxCount);

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.BeverageList);
            BeverageEnum beverageEnum = new BeverageEnum();
            List<String> beverages = beverageEnum.getList();
            listView = FindViewById<Android.Widget.ListView>(Resource.Id.listView);
            volumeGroup = FindViewById<RadioGroup>(Resource.Id.radioGroup1);
            volume = FindViewById<RadioButton>(volumeGroup.CheckedRadioButtonId);
            ArrayAdapter<String> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, beverages);
            listView.Adapter = adapter;
            String selectedItem;
            Beverage beverage = new Beverage();

            listView.ItemClick += (object sender, ItemClickEventArgs e) =>
            {
                selectedItem = listView.GetItemAtPosition(e.Position).ToString();
                beverage.setName(selectedItem);

                beverage.setDegree(beverageEnum.getDegree(beverage.getName()));
                if (volume.Text.Equals("50��"))
                {
                    beverage.setVolume(50);
                }
                else if (volume.Text.Equals("75��"))
                {
                    beverage.setVolume(75);

                }
                else if (volume.Text.Equals("100��"))
                {
                    beverage.setVolume(100);
                }
                else if (volume.Text.Equals("0.5�"))
                {
                    beverage.setVolume(500);
                }
                else
                {
                    beverage.setVolume(1000);
                }
                getDrunk.countEtanol(beverage);


                if (getDrunk.isEnough())
                {
                    Android.Graphics.Color color = new Android.Graphics.Color(255, 0, 255);
                    listView.SetBackgroundColor(color);
                    showDialog("�� ������� ������!!");

                    var activity = new Intent(this, typeof(MainActivity));
                    StartActivity(activity);
                }
                else if (getDrunk.isEnoughMid())
                {
                    beverages = beverageEnum.getWithoutMidBeverages();
                    adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, beverages);
                    listView.Adapter = adapter;
                    showDialog("����� ������");
                }
                else if (getDrunk.isEnoughStrong())
                {
                    beverages = beverageEnum.getWithoutStrongBeverages();
                    adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, beverages);
                    listView.Adapter = adapter;
                    showDialog("����-�� ������ ����� ������ ������");

                }
                else if (getDrunk.isEnoughHard())
                {
                    beverages = beverageEnum.getWithoutHardBeverages();
                    adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, beverages);
                    listView.Adapter = adapter;
                    showDialog("������ ����!");
                }

            };

        }
        public void showDialog(String message)
        {
            Android.App.AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog alertDialog = builder.Create();
            alertDialog.SetTitle("");
            // alertDialog.SetIcon(Android.Resource.Drawable.IcDialogInfo);
            alertDialog.SetMessage(message);
            alertDialog.Show();
            alertDialog.Wait(3000);
            alertDialog.Cancel();
        }

    }
}