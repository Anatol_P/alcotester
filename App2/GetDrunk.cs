using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App2.common
{
    class GetDrunk
    {
        private int maxCount;
        private int etanol;

        public GetDrunk(int maxCount)
        {
            this.maxCount = maxCount;
            this.etanol = 0;
        }

        public void countEtanol(Beverage beverage)
        {
            this.etanol += beverage.getEtanol();

        }
        public bool isEnough()
        {
            if (maxCount - etanol < 20)
            {
                return true;
            }
            else return false;

        }
        public bool isEnoughHard()
        {
            if (maxCount - etanol < 75)
            {
                return true;
            }
            else return false;

        }
        public bool isEnoughStrong()
        {
            if (maxCount - etanol < 41)
            {
                return true;
            }
            else return false;

        }
        public bool isEnoughMid()
        {
            if (maxCount - etanol < 25)
            {
                return true;
            }
            else return false;
        }
    }
 
}