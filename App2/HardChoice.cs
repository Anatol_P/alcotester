using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using App2.common;

namespace App2
{
    [Activity(Label = "���������! ����� �����?")]
    public class HardChoice : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {

            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.Drunkstage);

            Button button1 = FindViewById<Button>(Resource.Id.first);
            Button button2 = FindViewById<Button>(Resource.Id.second);
            Button button3 = FindViewById<Button>(Resource.Id.third);
            Button button4 = FindViewById<Button>(Resource.Id.fourth);
            Button button5 = FindViewById<Button>(Resource.Id.fifth);

            button1.Click += delegate
            {
                    var activity = new Intent(this, typeof(BeverageList));
                    activity.PutExtra("MyData", "50");
                    StartActivity(activity);
                };
            button2.Click += delegate
            {
                var activity = new Intent(this, typeof(BeverageList));
                activity.PutExtra("MyData", "90");
                StartActivity(activity);
            };
            button3.Click += delegate
            {
                var activity = new Intent(this, typeof(BeverageList));
                activity.PutExtra("MyData", "140");
                StartActivity(activity);
            };
            button1.Click += delegate
            {
                var activity = new Intent(this, typeof(BeverageList));
                activity.PutExtra("MyData", "190");
                StartActivity(activity);
            };
            button1.Click += delegate
            {
                var activity = new Intent(this, typeof(BeverageList));
                activity.PutExtra("MyData", "240");
                StartActivity(activity);
            };
        }
    }
}