using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App2.common
{
    class BeverageEnum {
        private List<String> strongBeverageEnum ;
        private List<String> hardBeverageEnum;
        private List<String> midBeverageEnum;
        private List<String> liteBeverageEnum;
        private List<String> beerBeverageEnum;
        public BeverageEnum()
        {
            this.strongBeverageEnum = new List<string>() { "�������", "����������", "������", "������", "�����", "�����",
                                     "������", "����", "���������", "������", "������", "��������", "���", "������" };
            this.hardBeverageEnum = new List<string>() { "������", "�������", "�������" };
            this.midBeverageEnum = new List<string>() {"���������", "������", "�����", "�������", "����",
                                                    "��������", "����", "�����"};
            this.liteBeverageEnum = new List<string>() { "����" };
            this.beerBeverageEnum = new List<string>() { "�������", "��������", "����" };
         }
   
       
        public List<String> getList()
        {
            List<String> beverageList = new List<string>();
            beverageList.AddRange(strongBeverageEnum);
            beverageList.AddRange(hardBeverageEnum);
            beverageList.AddRange(midBeverageEnum);
            beverageList.AddRange(liteBeverageEnum);
            beverageList.AddRange(beerBeverageEnum);
            beverageList.Sort();
            return beverageList;
             }

        public int getDegree(String name)
        {
            if (this.strongBeverageEnum.Contains(name))
                return 40;
            else if (this.midBeverageEnum.Contains(name))
                return 20;
            else if (this.liteBeverageEnum.Contains(name))
                return 12;
            else if (this.beerBeverageEnum.Contains(name))
                return 5;
            else if (this.hardBeverageEnum.Contains(name))
                return 70;

            return 0;
        }
        public List<String> getWithoutHardBeverages()
        {
            List<String> beverageList = new List<string>();
            beverageList.AddRange(strongBeverageEnum);
            beverageList.AddRange(midBeverageEnum);
            beverageList.AddRange(liteBeverageEnum);
            beverageList.AddRange(beerBeverageEnum);
            beverageList.Sort();
            return beverageList;
        }
        public List<String> getWithoutStrongBeverages()
        {
            List<String> beverageList = new List<string>();
            beverageList.AddRange(midBeverageEnum);
            beverageList.AddRange(liteBeverageEnum);
            beverageList.AddRange(beerBeverageEnum);
            beverageList.Sort();
            return beverageList;
        }
        public List<String> getWithoutMidBeverages()
        {
            List<String> beverageList = new List<string>();
            beverageList.AddRange(liteBeverageEnum);
            beverageList.AddRange(beerBeverageEnum);
            beverageList.Sort();
            return beverageList;
        }
    }
}
