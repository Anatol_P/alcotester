using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App2.common
{
    class Beverage
    {
        private String name;
        private int degree;
        private int volume;
       
        public String getName()
        {
            return this.name;
        }
        public void setName(String name)
        {
            this.name = name;
        }

        public int getDegree()
        {
            return this.degree;
        }
        public void setDegree(int degree)
        {
            this.degree = degree;
        }

        public int getVolume()
        {
            return this.volume;
        }
        public void setVolume(int volume)
        {
            this.volume = volume;
        }
        public int getEtanol()
        {
            double etanol = (this.getVolume() * this.getDegree()) / 100;
            return (int) etanol;
        }

    }
}